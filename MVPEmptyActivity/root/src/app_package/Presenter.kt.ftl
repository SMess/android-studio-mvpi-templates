package ${packageName}

import com.smess.basemvpandroidarchitecture.ui.BasePresenter
import com.smess.basemvpandroidarchitecture.ui.MVPContract

class ${className}Presenter(viewContract: ${className}Contract.ViewContract) : BasePresenter<MVPContract.ViewContract, MVPContract.InteractorContract>(viewContract),
        ${className}Contract.PresenterContract.View,
        ${className}Contract.PresenterContract.Interactor {

    companion object {
        private val TAG = ${className}Presenter::class.java.name
    }

    override fun createInteractor(P: BasePresenter<MVPContract.ViewContract, MVPContract.InteractorContract>): MVPContract.InteractorContract {
        return ${className}Interactor(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
    }
}