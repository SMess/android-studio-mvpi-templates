package ${packageName}

import com.smess.basemvpandroidarchitecture.ui.BaseInteractor


class ${className}Interactor(presenter: ${className}Presenter) : BaseInteractor<${className}Contract.PresenterContract.Interactor>(presenter), ${className}Contract.InteractorContract {

    companion object {
        private val TAG = ${className}Interactor::class.java.name
    }

}