package ${packageName}


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smess.basemvpandroidarchitecture.ui.BaseFragment

class ${className}Fragment : BaseFragment<${className}Contract.PresenterContract>(), ${className}Contract.ViewContract {

    companion object {
        private val TAG = ${className}Fragment::class.java.name

        fun newInstance(): ${className}Fragment {
            val fragment = ${className}Fragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        presenter.setBundle(this.arguments)
        super.onCreateView(inflater, container, savedInstanceState)

		<#if includeLayout>
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.${fragmentName}, container, false);
		<#else>
        TextView textView = new TextView(getActivity());
        textView.setText(R.string.hello_blank_fragment);
        return textView;
		</#if>
    }

    
    override fun createPresenter(): ${className}Contract.PresenterContract {
        return ${className}Presenter(this)
    }

    override fun assignTag(): String = TAG

}
