package ${packageName}

import com.smess.basemvpandroidarchitecture.ui.MVPContract


interface ${className}Contract {

    interface ViewContract : MVPContract.ViewContract {
        //functions implemented by the view and called from the presenter goes here 
    }

    interface PresenterContract : MVPContract.PresenterContract {
        interface View : PresenterContract {
            //functions implemented by the presenter and called from the view goes here 
            
        }

        interface Interactor : PresenterContract {
            //functions implemented by the presenter and called from the interactor goes here 
            
        }
    }

    interface InteractorContract : MVPContract.InteractorContract {
        //functions implemented by the interactor and called from the presenter goes here 
        
    }
}