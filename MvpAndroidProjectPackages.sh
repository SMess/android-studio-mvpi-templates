#!/bin/bash

#This will generate all empty packages needed by the Mvp Architecture
#Copy/past the file "MvpAndroidProjectPackages.bash" in root folder of the project (in the same level as app folder)
#Run the script as bellow:
#Pass the android package name as param (replace dots by "/"  ex : "bash MvpAndroidProjectPackages.bash com/example/myapp")

read -p 'Project path : ' projectPath
read -p 'package name : ' package

packagePath=${package//./\/}

mkdir "$projectPath/app/src/integ"
mkdir "$projectPath/app/src/integ/assets"
mkdir "$projectPath/app/src/integ/res"

mkdir "$projectPath/app/src/prod"
mkdir "$projectPath/app/src/prod/assets"
mkdir "$projectPath/app/src/prod/res"

mkdir "$projectPath/app/src/main/java/$packagePath/application"

mkdir "$projectPath/app/src/main/java/$packagePath/domain"
mkdir "$projectPath/app/src/main/java/$packagePath/domain/models"
mkdir "$projectPath/app/src/main/java/$packagePath/domain/workers"

mkdir "$projectPath/app/src/main/java/$packagePath/scenes"
mkdir "$projectPath/app/src/main/java/$packagePath/scenes/main"

mkdir "$projectPath/app/src/main/java/$packagePath/services"
mkdir "$projectPath/app/src/main/java/$packagePath/sp"
mkdir "$projectPath/app/src/main/java/$packagePath/storage"
mkdir "$projectPath/app/src/main/java/$packagePath/utils"

