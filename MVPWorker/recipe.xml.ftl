<?xml version="1.0"?>
<recipe>

	<instantiate from="src/app_package/Callback.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${className}WorkerCallback.kt" />

    <instantiate from="src/app_package/Worker.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${className}Worker.kt" />


	<open file="${srcOut}/${className}Worker.kt"/>
</recipe>