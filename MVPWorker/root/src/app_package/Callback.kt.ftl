package ${packageName}

import com.smess.basemvpandroidarchitecture.workers.BaseWorkerCallback


interface ${className}WorkerCallback : BaseWorkerCallback {
    
}